'use strict';

var array = [1, 2, 3, 4, 5, 6];

var oddNumbers = myFilter(array, function (each) {
    return each % 2 === 1;
});

console.log('Odd numbers: ' + oddNumbers);

// filter out and return elements that
// satisfy the predicate function
function myFilter(list, predicate) {
    var result = [];

    for (var i = 0; i < list.length; i++) {
        var current = list[i];
        if (predicate(current)) {
            result.push(current);
        }
    }
    return result;
}