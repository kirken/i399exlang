'use strict';

var a = 1;
var b = 2;
var c = 4;
var d = 5;

console.log(a);

add(1, 1, function (sum1) {
    console.log(b);
    add(sum1, 1, function (sum2) {
        console.log(sum2);
    });
    console.log(c);
});

console.log(d);

function add(x, y, callback) {
    callback(x + y);
}
