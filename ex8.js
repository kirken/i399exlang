'use strict';

var a = 1;
var b = 3;
var c = 4;
var d = 2;

console.log(a);

add(1, 1, function (sum1) {
    console.log(b);
    add(sum1, 3, function (sum2) {
        console.log(sum2);
    });
    console.log(c);
});

console.log(d);

function add(x, y, callback) {
    setTimeout(callback, 0, x + y);
}
