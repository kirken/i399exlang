'use strict';

var array = [
    { id: 1, selected: false },
    { id: 2, selected: true },
    { id: 3, selected: false },
    { id: 4, selected: true },
    { id: 5, selected: true },
    { id: 6, selected: false }
];

array = array.filter(function (each) {
    return each.selected;
}).map(function (each) {
    return each.id;
});

console.log('Ids of selected objects: ' + array);

