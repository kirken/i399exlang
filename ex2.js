'use strict';

var array = [1, 2, 3, 4, 5, 6];
array.push(7);
array.push(8);

array = array.filter(function (each) {
    return each % 2 === 0;
}).map(function (each) {
    return each * each;
});

console.log('Even numbers squared: ' + array);

