'use strict';
//(2 + 3) * 6
add(2,3, function (result1){
    multiply(result1,6, function(result2){
        console.log(result2);
    });
});

//(1 + 2) * (2 + 3)
add(1,2, function (sum1){
    add(2,3, function(sum2){
        multiply(sum1,sum2, function(result){
            console.log(result);
        });
    });
});

//(1 + 2) * (2 + 3) * (3 + 4)
add(1,2, function (sum1){
    add(2,3, function(sum2){
        add(3,4, function(sum3){
            multiply(sum1,sum2, function(mult1){
                multiply(mult1,sum3, function(result){
                    console.log(result);
                });
            });
        });
    });
});


function add(x, y, callback) {
    callback(x + y);
}

function multiply(x, y, callback) {
    callback(x * y);
}
